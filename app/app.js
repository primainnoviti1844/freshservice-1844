$(document).ready(function() {
	app.initialized().then(function(_client) {
		var client = _client;
		client.events.on('ticket.propertiesUpdated', function(event) {
			client.data
				.get('ticket')
				.then(function(data) {

					var event_data = event.helper.getData();
					client.data.get('loggedInUser').then(userData => {
						let args = {
							ticketId: data.ticket.display_id,
							updatedFields: event_data,
							updatedBy: userData.loggedInUser.user,
						};
						client.request.invoke('syncTicket', args).then(
							function(response) {
								// console.log(JSON.stringify(response));
							},
							function(err) {
								console.log("Error occurred while invoking syncTicket",JSON.stringify(err));
							}
						);
					});
				})
				.catch(function(e) {
					console.log('Unknown error occurred', JSON.stringify(e));
				});
		});
	});
});
