const request = require('request');
const _ = require('lodash');
const { getTicket, getApiConfig } = require('./service/FreshdeskService');

exports = {
	events: [{ event: 'onTicketCreate', callback: 'onTicketCreateHandler' }],

	// args is a JSON block containing the payload information.
	// args['iparam'] will contain the installation parameter values.
	onTicketCreateHandler: function(args) {
		const url = 'https://freshdesk.innoviti.com:8443/fd_data_api/summary_api/index.php/createTicketData';
		this.syncTicket({ ticketId: args.data.ticket.id, iparams: args['iparams'], url: url });
	},

	syncTicket: function(args) {
		const isCreateEvent = args.url ? true : false;
		if (!args.url) {
			args['url'] = 'https://freshdesk.innoviti.com:8443/fd_data_api/summary_api/index.php/updateTicketData';
		}
		let updatedFields = args.updatedFields;
		const iparams = args.iparams;
		const updatedBy = args.updatedBy;
		console.log('syncTicket', updatedFields, 'updatedBy:', updatedBy);

		if (updatedFields) {
			_.forEach(_.keys(updatedFields), keyx => {
				updatedFields[keyx] = updatedFields[keyx].value;
			});
		}
		const ticketId = args.ticketId;
		let auth = {
			username: iparams['InnovitiUsername'],
			password: iparams['InnovitiPassword'],
		};
		const apiConfig = getApiConfig(iparams);
		let ticketPromise = getTicket(apiConfig, ticketId, { include: 'requester,stats' });
		ticketPromise.then(data => {
			data.ticket.updatedFields = updatedFields;
			if (updatedBy) {
				data.ticket.updaterDetails = {
					name: updatedBy.name,
					email: updatedBy.email,
				};
			}
			request.post(
				args.url,
				{
					body: data,
					auth: auth,
					json: true,
				},
				function(error, response, body) {
					if (error || (response && response.statusCode >= 300)) {
						console.log('Error occurred:', JSON.stringify(error), "response", JSON.stringify(response),"body", JSON.stringify(body));
					}else if(!isCreateEvent){
						console.log("renderData called");
						renderData();
					}
				}
			);
		});
		
	},
};
